import logging
from dataclasses import dataclass

import psycopg2


@dataclass
class PostgresConf:
    host: str
    port: int
    database: str
    user: str
    password: str

    def __repr__(self) -> str:
        return f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}"


class PostgresClient:
    def __init__(self, conn):
        self.postgres_conf = PostgresConf(
            user=conn.info.user,
            password=conn.info.password,
            port=conn.info.port,
            database=conn.info.dbname,
            host=conn.info.host,
        )
        self.conn = conn

    @classmethod
    def from_conf(cls, postgres_conf: PostgresConf):
        conn = psycopg2.connect(dsn=str(postgres_conf))
        return cls(conn)

    def run(self, sql) -> None:
        with self.conn.cursor() as cur:
            cur.execute(sql)
            logging.info("%s row affected -> %s", str(cur.rowcount), sql)
            return cur.rowcount

    def bulk_export(self, query, file) -> None:
        with self.conn.cursor() as cur:
            with open(file, "w") as file:
                cur.copy_expert(f"COPY ({query})  TO STDOUT WITH CSV HEADER", file)

    def bulk_load(self, table, path) -> None:
        logging.info("loading %s from %s", table, path)
        with self.conn.cursor() as cur:
            with open(path, "r", buffering=8192) as file:
                cur.copy_expert(f'COPY "{table}"  FROM STDIN WITH CSV HEADER', file)

    def bulk_export_solr(self, query: str, file, sep1: str = "\t") -> None:
        with self.conn.cursor() as cur:
            with open(file, "w") as file:
                cur.copy_expert(
                    f"COPY ({query})  TO STDOUT CSV HEADER DELIMITER '{sep1}' ENCODING 'UTF8' NULL AS ''",
                    file,
                )
