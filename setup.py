from setuptools import find_packages, setup

from pysolrwrapper import __version__

setup(
    name="pysolrwrapper",
    version=__version__,
    url="https://github.com/mypackage.git",
    author="Author Name",
    author_email="author@gmail.com",
    description="Description of my package",
    packages=find_packages(),
    install_requires=[],
)
